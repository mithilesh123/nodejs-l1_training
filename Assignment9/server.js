const net = require('net');

var server = net.createServer(function(conn){
  console.log('Client connected..');

  conn.on('end',function(){
    console.log('Client Disconnected..');
  })

  conn.write('Hi From topgear\n');
  conn.pipe(conn);
})


server.listen(5000,function(){
  console.log('Server is listening.......');
})
