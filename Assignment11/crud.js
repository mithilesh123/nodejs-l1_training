const MongoClient = require('mongodb').MongoClient;

var url = "mongodb://localhost:27017/";

var settings = {
      reconnectTries : Number.MAX_VALUE,
      autoReconnect : true
};

var newObj = {"name":"mithilesh pradhan","assignment":"topgear node js"}

MongoClient.connect(url,settings,function(err,db) {

    if(err) throw err;

    var dbs = db.db('topgear123');

   //CREATE OPERATION
   function creatOperation(){

     dbs.collection('crud').insertOne(newObj,function(err,res){
       if(err) throw err;

       console.log("1 document inserted");
       console.log(res);

       db.close();

     })


   }

   //READ OPERATION
   function readOperation() {

     dbs.collection('crud').findOne({},function(err,res){
       if(err) throw err;

       console.log("Record found: ");
       console.log(res.ops);

       db.close();

     })
   }

   //UPDATE OPERATION
   function updateOperation() {

    var myquery = { assignment: "topgear node js" };
    var newvalues = { $set: {name: "Mithilesh Pradhan", assignment: "mongo crud" } };
    dbs.collection("crud").updateOne(myquery, newvalues, function(err, res) {
    if (err) throw err;
    console.log("1 document updated");
    db.close();
  });
   }


   function deleteOperation() {

    var myquery = { assignment: "mongo crud" };
    dbs.collection("crud").deleteOne(myquery,function(err, res) {
    if (err) throw err;
    console.log("1 document deleted");
    db.close();
  });
   }

 creatOperation();
 readOperation();
 updateOperation();
 deleteOperation();

})
