const http = require('http');

http.createServer(function(req,res){
  res.writeHead(200,{'Content-Type':'text/html'});
  res.write('<h1>Hello From TopGear</h1>');
  res.end();
}).listen(8000,console.log("Server listening on port 8000"));
