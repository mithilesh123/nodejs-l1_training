const fs = require('fs');

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

readline.question(`What's your file name?`, (name) => {
  fs.readFile(name,'utf-8',function(err,contents){
    console.log(contents);
  })
  readline.close()
})
